package com.jenya1.eattendance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.example.jenya1.eattendance.uploadutil.MySingleton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LoginActivity extends AppCompatActivity implements SurfaceHolder.Callback  {
    private Context mContext=LoginActivity.this;

    private EditText inputEmail;// inputPassword;
    TextView title,actionbartitle,conformation;
    ImageView actionbarimg;
    private ProgressBar progressBar;
    //private Button btnSignup, btnLogin, btnReset;

    String email,date,name,imageFilePath,department,allow_outside;
    String activity,workstatus;

    //http://www.rayvatapps.com:8001/ school

    //http://192.168.1.3:9002/ jenya
    String URL_LOGIN = "http://192.168.1.3:9002/api.php?request=login";//http://192.168.1.3:9002/api.php?request=login //http://192.168.1.144:8101/v2/api.php?request=login
//    String URL_LOGIN = "http://192.168.1.144:8061/api.php?request=login";//http://192.168.1.3:9002/api.php?request=login //http://192.168.1.144:8101/v2/api.php?request=login

//    String URL_WORK = "http://192.168.1.144:8101/api.php?request=attendance";

    String URL_WORK_START ="http://192.168.1.3:9002/api.php?request=startjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_WORK_START ="http://192.168.1.144:8061/api.php?request=startjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_WORK_END ="http://192.168.1.3:9002/api.php?request=endjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_WORK_END ="http://192.168.1.144:8061/api.php?request=endjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_LUNCH_START ="http://192.168.1.3:9002/api.php?request=startlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_LUNCH_START ="http://192.168.1.144:8061/api.php?request=startlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_LUNCH_END ="http://192.168.1.3:9002/api.php?request=endlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_LUNCH_END ="http://192.168.1.144:8061/api.php?request=endlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_TEA_START ="http://192.168.1.3:9002/api.php?request=starttea";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_TEA_START ="http://192.168.1.144:8061/api.php?request=starttea";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_TEA_END ="http://192.168.1.3:9002/api.php?request=endtea";//http://192.168.1.3:9002//192.168.1.144:8101/v2
//    String URL_TEA_END ="http://192.168.1.144:8061/api.php?request=endtea";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    LinearLayout login,lnin,lnout,lnimg,lnoperation,lnemoji,lnedlogin;//lnpreview
    TextView in,out;

    //surface
    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude=" ",longitude=" ";

    private ImageView showImageView,emoji;//imgarrow

    boolean previewing = false;

    Bitmap image;
    int count=0;
    String imagename;

    int stlog=0,stlunch=0,sttea=0,logcheck=0;

    int stlogout=0,stlunchout=0,stteaout=0;

    int maxLength;
    private int min=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_update);

         Bundle extras = getIntent().getExtras();
        if(extras == null) {
            activity="0000";
        } else {
            activity= extras.getString("activity");
            //Toast.makeText(this, ""+activity, Toast.LENGTH_SHORT).show();
            Log.e("activity ",activity);
        }

        //LoginActivity.this.setTitle(activity);

        inputEmail = (EditText) findViewById(R.id.email);
//        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        title=(TextView)findViewById(R.id.email);
        conformation=(TextView)findViewById(R.id.tv_conformation);
//        login=(LinearLayout)findViewById(R.id.lnlogin);
        lnoperation=(LinearLayout)findViewById(R.id.lnoperation);
//        lnpreview=(LinearLayout)findViewById(R.id.lnpreview);
        lnedlogin=(LinearLayout)findViewById(R.id.lnedlogin);
        lnemoji=(LinearLayout)findViewById(R.id.lnemoji);
        lnin=(LinearLayout)findViewById(R.id.lnin);
        lnout=(LinearLayout)findViewById(R.id.lnout);
        lnimg=(LinearLayout)findViewById(R.id.lnview);
        showImageView = (ImageView) findViewById(R.id.img_preview);
        emoji = (ImageView) findViewById(R.id.imgemoji);
        actionbartitle=(TextView)findViewById(R.id.toolbar_subtitle);
        actionbarimg = (ImageView) findViewById(R.id.media_image);
//        imgarrow = (ImageView) findViewById(R.id.img_arrow);

        in=(TextView)findViewById(R.id.btn_checkin);
        out=(TextView)findViewById(R.id.btn_checkout);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        inputEmail.setTypeface(tf);
        conformation.setTypeface(tf);
        title.setTypeface(tf);
        in.setTypeface(tf);
        out.setTypeface(tf);

       /* Glide.with(LoginActivity.this)
                .load(R.drawable.click)
                .into(imgarrow);*/

        /*LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.popupdialog, null);
        SurfaceView dialogsurface=(SurfaceView)alertLayout.findViewById(R.id.dialogcamerapreview);
//                final Button picture=(Button)alertLayout.findViewById(R.id.btntakepicture);
        final LinearLayout lnin=(LinearLayout)alertLayout.findViewById(R.id.lnin);
        final LinearLayout lnno=(LinearLayout)alertLayout.findViewById(R.id.lnout);
//        surfaceHolder = dialogsurface.getHolder();
//        surfaceHolder.addCallback(LoginActivity.this);
//        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
//                alert.setTitle("Info");
//                alert.setMessage(activity);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        alert.show();*/
        int img=0;

        if(activity.equals("Work"))
        {
            img=R.drawable.work;
        }
        else if(activity.equals("Break"))
        {
            img=R.drawable.lunch;
        }
        else if(activity.equals("Tea Break"))
        {
            img=R.drawable.tea;
        }

        actionbartitle.setText(activity);
        actionbarimg.setBackgroundResource(img);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().setNavigationBarColor(getResources().getColor(R.color.white));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        }

        surfaceView = (SurfaceView)findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

         maxLength = 4;
                inputEmail.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if(inputEmail.getText().toString().length()>= maxLength) {
                            //Show toast here
                            email = inputEmail.getText().toString().trim();

                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(inputEmail.getWindowToken(), 0);
                            // password = inputPassword.getText().toString().trim();

                            if (TextUtils.isEmpty(email)) {
                                inputEmail.setError("Enter email address!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                                return false;
                            }

               /* if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                            progressBar.setVisibility(View.VISIBLE);

                            checklogin();

//                            login.setEnabled(false);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    login.setEnabled(true);
                                }
                            }, 3000);
                        }
                        return false;
                    }
                });

        lnin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputEmail.getWindowToken(), 0);

                lnin.setVisibility(View.GONE);
                lnout.setVisibility(View.GONE);
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.popupdialog, null);
                SurfaceView dialogsurface=(SurfaceView)alertLayout.findViewById(R.id.dialogcamerapreview);
                final TextView tvWorkstatus=(TextView) alertLayout.findViewById(R.id.tvworkstatus);
                final LinearLayout lnindialog=(LinearLayout)alertLayout.findViewById(R.id.lnindialog);
                final LinearLayout lnnodialog=(LinearLayout)alertLayout.findViewById(R.id.lnoutdialog);
                surfaceHolder = dialogsurface.getHolder();
                surfaceHolder.addCallback(LoginActivity.this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                final AlertDialog.Builder alert = new AlertDialog.Builder(mContext,R.style.dialog);

                alert.setView(alertLayout);

                alert.setCancelable(false);
                tvWorkstatus.setText(workstatus);
                lnindialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(logcheck==1)
                        {

                            if(activity.equals("Work"))
                            {
                                if(stlog==1) {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count = 1;
                                        lnindialog.setEnabled(false);

                                    }
                                }
                                else
                                {
                                    if(stlogout==1)
                                    {

                                        Log.e("min", String.valueOf(min));
                                        if(min==1)
                                        {
                                            Toast.makeText(LoginActivity.this, "wait 10 minute to logout", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            if (previewing) {
                                                camera.takePicture(null, null, null, jpegCallback);
                                                count=1;
                                                lnindialog.setEnabled(false);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            else if(activity.equals("Break"))
                            {
                                if(stlunch==1) {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count = 1;
                                        lnindialog.setEnabled(false);
                                    }
                                }
                                else
                                {
                                    if(stlog!=1)
                                    {
                                        Toast.makeText(LoginActivity.this, "Start Job First", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if(stlunchout==1)
                                    {
                                        if (previewing) {
                                            camera.takePicture(null, null, null, jpegCallback);
                                            count=1;
                                            lnindialog.setEnabled(false);
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            else if(activity.equals("Tea Break"))
                            {
                                if(sttea==1)
                                {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count=1;
                                        lnindialog.setEnabled(false);
                                    }
                                }
                                else
                                {
                                    if(stlog!=1)
                                    {
                                        Toast.makeText(LoginActivity.this, "Start Job First", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if(stteaout==1)
                                    {
                                        if (previewing) {
                                            camera.takePicture(null, null, null, jpegCallback);
                                            count=1;
                                            lnindialog.setEnabled(false);
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "Login please...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                final AlertDialog dialog = alert.create();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.NO_GRAVITY;
//                lp.windowAnimations = R.style.DialogAnimation;
                dialog.getWindow().setAttributes(lp);

                lnnodialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.hide();
                        Intent i=new Intent(LoginActivity.this,LoginActivity.class);
                        i.putExtra("activity", activity);
                        startActivity(i);
                        finish();
                    }
                });
                dialog.show();
//                alert.show();

                lnin.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //lnin.setEnabled(true);
                    }
                }, 3000);

            }
        });
        lnout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputEmail.getWindowToken(), 0);

//                int check=checkstatusout();

                if(logcheck==1)
                {
                    Intent i=new Intent(LoginActivity.this,LoginActivity.class);
                    i.putExtra("activity", activity);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Login please...", Toast.LENGTH_SHORT).show();
                }

                lnout.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lnout.setEnabled(true);
                    }
                }, 3000);

            }
        });

        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lnin.setVisibility(View.GONE);
                lnout.setVisibility(View.GONE);
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.popupdialog, null);
                SurfaceView dialogsurface=(SurfaceView)alertLayout.findViewById(R.id.dialogcamerapreview);
                final TextView tvWorkstatus=(TextView) alertLayout.findViewById(R.id.tvworkstatus);
                final LinearLayout lnindialog=(LinearLayout)alertLayout.findViewById(R.id.lnindialog);
                final LinearLayout lnnodialog=(LinearLayout)alertLayout.findViewById(R.id.lnoutdialog);
                surfaceHolder = dialogsurface.getHolder();
                surfaceHolder.addCallback(LoginActivity.this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                final AlertDialog.Builder alert = new AlertDialog.Builder(mContext, R.style.dialog);

                alert.setView(alertLayout);

                alert.setCancelable(false);

                tvWorkstatus.setText(workstatus);
                /*alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                    }
                });*/


                lnindialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(logcheck==1)
                        {

                            if(activity.equals("Work"))
                            {
                                if(stlog==1) {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count = 1;
                                        lnindialog.setEnabled(false);
                                    }
                                }
                                else
                                {
                                    if(stlogout==1)
                                    {

                                        Log.e("min", String.valueOf(min));
                                        if(min==1)
                                        {
                                            Toast.makeText(LoginActivity.this, "wait 10 minute to logout", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            if (previewing) {
                                                camera.takePicture(null, null, null, jpegCallback);
                                                count=1;
                                                lnindialog.setEnabled(false);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            else if(activity.equals("Break"))
                            {
                                if(stlunch==1) {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count = 1;
                                        lnindialog.setEnabled(false);
                                    }
                                }
                                else
                                {
                                    if(stlog!=1)
                                    {
                                        Toast.makeText(LoginActivity.this, "Start Job First", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if(stlunchout==1)
                                    {
                                        if (previewing) {
                                            camera.takePicture(null, null, null, jpegCallback);
                                            count=1;
                                            lnindialog.setEnabled(false);
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            else if(activity.equals("Tea Break"))
                            {
                                if(sttea==1)
                                {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count=1;
                                        lnindialog.setEnabled(false);
                                    }
                                }
                                else
                                {
                                    if(stlog!=1)
                                    {
                                        Toast.makeText(LoginActivity.this, "Start Job First", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if(stteaout==1)
                                    {
                                        if (previewing) {
                                            camera.takePicture(null, null, null, jpegCallback);
                                            count=1;
                                            lnindialog.setEnabled(false);
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(LoginActivity.this, "Already Ended...", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "Login please...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                final AlertDialog dialog = alert.create();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.NO_GRAVITY;
//                lp.windowAnimations = R.style.DialogAnimation;
                dialog.getWindow().setAttributes(lp);

                lnnodialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.hide();
                        Intent i=new Intent(LoginActivity.this,LoginActivity.class);
                        i.putExtra("activity", activity);
                        startActivity(i);
                        finish();
                    }
                });
                dialog.show();
//                alert.show();

                in.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in.setEnabled(true);
                        lnin.setEnabled(true);
                    }
                }, 3000);

            }
        });


        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                int check=checkstatusout();

                if(logcheck==1)
                {
                    Intent i=new Intent(LoginActivity.this,LoginActivity.class);
                    i.putExtra("activity", activity);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Login please...", Toast.LENGTH_SHORT).show();
                }
                /*if(in.isEnabled())
                {
                    *//*camera.takePicture(null, null, null, jpegCallback);
                    count=2;*//*
                    //outprocess();
                }*/

            }
        });
    }
    protected void showCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

    }

    @SuppressLint("LongLogTag")
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

                /*Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);

            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

                /*Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);



            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));

//                textView.setText("Your current location is"+ "\n" + "Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

               /* Toast.makeText(this, "Your current location is"+"\n" + "Lattitude = " + lattitude +
                        "\n" + "Longitude = " + longitude, Toast.LENGTH_LONG).show();*/

                Log.e("Your current location" ,"Lattitude = " + lattitude
                        + "\n" + "Longitude = " + longitude);

            }else{

                longitude="76.76";
                lattitude="23.23";
            }

        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {

            FileOutputStream outStream = null;
            try {
                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance");

                if (!direct.exists()) {
                    direct.mkdirs();
                }
                name=email+ System.currentTimeMillis()+".jpg";
                imageFilePath = direct+"/"+name;
                imagename=name;
                Uri selectedImage = Uri.parse(imageFilePath);
                File file = new File(imageFilePath);
                String path = file.getAbsolutePath();
                Bitmap bitmap = null;
                // Directory and name of the photo. We put system time
                // as a postfix, so all photos will have a unique file name.
                outStream = new FileOutputStream(file);
                outStream.write(data);
                outStream.close();

                if (path != null) {
                    if (path.startsWith("content")) {
                        bitmap = decodeStrem(file, selectedImage,
                                LoginActivity.this);
                    } else {
                        String manufacturer = Build.MANUFACTURER;
                        if (manufacturer.toLowerCase().contains("xiaomi")) {
                            bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        } else {
                            bitmap = decodeFile(file, 10); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }
//                        bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                    }
                    Log.e("bitmap", String.valueOf(bitmap));
                }
                if (bitmap != null) {
                    image=bitmap;
//                    image=getResizedBitmap(bitmap,512,512);
                    Toast.makeText(LoginActivity.this,
                            "Picture Captured Successfully", Toast.LENGTH_LONG)
                            .show();

                    operation();

                } else {
                    Toast.makeText(LoginActivity.this,
                            "Failed to Capture the picture. kindly Try Again:",
                            Toast.LENGTH_LONG).show();
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }

        }
    };

    private void operation() {
        /*LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.popupdialoggif, null);
        SurfaceView dialogsurface=(SurfaceView)alertLayout.findViewById(R.id.dialogcamerapreview);
//                final Button picture=(Button)alertLayout.findViewById(R.id.btntakepicture);
//        final ImageView emoji=(ImageView) alertLayout.findViewById(R.id.imgemoji);
        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(true);*/
//        final AlertDialog dialog = alert.create();
        final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.setContentView(R.layout.popupdialoggif);
        final ImageView emoji=(ImageView) dialog.findViewById(R.id.imgemoji);

        if(activity.equals("Work"))
        {
            if(stlog==1)
            {
                dialog.show();
                Glide.with(LoginActivity.this)
                        .load(R.drawable.welcome)
                        .into(emoji);
//                FILE_UPLOAD_URL=;
//                new UploadFileToServer(path).execute();
                checkwork(URL_WORK_START, image);
            }
            else
            {
                            /*camera.takePicture(null, null, null, jpegCallback);
                            count=0;
                            lnin.setEnabled(false);*/

                if(stlogout==1)
                {
                    dialog.show();
                    Glide.with(LoginActivity.this)
                            .load(R.drawable.job_end)
                            .into(emoji);

//                    new UploadFileToServer(path).execute();
                    checkwork(URL_WORK_END, image);
                }
                else {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(activity.equals("Break"))
        {
            if(stlunch==1)
            {
                dialog.show();
                Glide.with(LoginActivity.this)
                        .load(R.drawable.lunchop)
                        .into(emoji);
                checkwork(URL_LUNCH_START, image);

            }
            else
            {

                if(stlunchout==1)
                {
                    dialog.show();
                    Glide.with(LoginActivity.this)
                            .load(R.drawable.lunchop)
                            .into(emoji);
                    checkwork(URL_LUNCH_END, image);
                }
                else
                {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }

            }
        }
        else if(activity.equals("Tea Break"))
        {
            if(sttea==1)
            {
                dialog.show();
                Glide.with(LoginActivity.this)
                        .load(R.drawable.teaemoji)
                        .into(emoji);
                checkwork(URL_TEA_START, image);

            }
            else
            {
                if(stteaout==1)
                {
                    dialog.show();
                    Glide.with(LoginActivity.this)
                            .load(R.drawable.teaemoji)
                            .into(emoji);
                    checkwork(URL_TEA_END, image);
                }
                else
                {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public Bitmap decodeStrem(File fil, Uri selectedImage, Context mContext) {

        Bitmap bitmap = null;
        try {

            bitmap = BitmapFactory.decodeStream(mContext.getContentResolver()
                    .openInputStream(selectedImage));
            /*Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
            img = ConvertBitmapToString(resizedBitmap);*/
            final int THUMBNAIL_SIZE = getThumbSize(bitmap);

            bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE,
                    THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baos
                    .toByteArray()));

            return bitmap = rotateImage(bitmap, fil.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap decodeFile(File f, int sampling) {
        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);

            o2.inSampleSize = sampling;
            o2.inTempStorage = new byte[48 * 1024];

            o2.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            bitmap = rotateImage(bitmap, f.getAbsolutePath());
            //img = ConvertBitmapToString(bitmap);

            return bitmap;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap rotateImage(Bitmap bmp, String imageUrl) {
        if (bmp != null) {
            ExifInterface ei;
            int orientation = 0;
            try {
                ei = new ExifInterface(imageUrl);
                orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

            } catch (IOException e) {
                e.printStackTrace();
            }
            int bmpWidth = bmp.getWidth();
            int bmpHeight = bmp.getHeight();
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED:
                    matrix.postRotate(270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }
            Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmpWidth,
                    bmpHeight, matrix, true);
            return resizedBitmap;
        } else {
            return bmp;
        }
    }

    public static int getThumbSize(Bitmap bitmap) {

        int THUMBNAIL_SIZE = 250;
        if (bitmap.getWidth() < 300) {
            THUMBNAIL_SIZE = 250;
        } else if (bitmap.getWidth() < 600) {
            THUMBNAIL_SIZE = 500;
        } else if (bitmap.getWidth() < 1000) {
            THUMBNAIL_SIZE = 750;
        } else if (bitmap.getWidth() < 2000) {
            THUMBNAIL_SIZE = 1500;
        } else if (bitmap.getWidth() < 4000) {
            THUMBNAIL_SIZE = 2000;
        } else if (bitmap.getWidth() > 4000) {
            THUMBNAIL_SIZE = 2000;
        }
        return THUMBNAIL_SIZE;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 3000;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        TextView txt = (TextView) findViewById(R.id.usage);
                        if(conformation.getVisibility() == View.VISIBLE){
                            conformation.setVisibility(View.INVISIBLE);
                        }else{
                            conformation.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }

    private void checklogin() {
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            //Log.e("request",ServerResponse);
                            JSONObject jObj = new JSONObject(ServerResponse);

                            String status=jObj.getString("status");

                            String msg =jObj.getString("message");

                            if (status.equals("1")) {

                                conformation.setVisibility(View.VISIBLE);
                                lnoperation.setVisibility(View.VISIBLE);
                                lnin.setEnabled(true);
                                lnout.setEnabled(true);
                                in.setEnabled(true);
                                out.setEnabled(true);
                                logcheck=1;
                                //Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                conformation.setVisibility(View.GONE);
                                lnoperation.setVisibility(View.GONE);
                                lnin.setEnabled(true);
                                lnout.setEnabled(true);
                                in.setEnabled(false);
                                out.setEnabled(false);

                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);

                            getUsername();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                date=sdf.format(new Date());
                // Adding All values to Params.

                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }


    private void getUsername() {

        String JSON_USERNAME=URL_LOGIN+"&username="+email+"&date="+date;
        Log.e("da",JSON_USERNAME);
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_USERNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("response");

                            String uname=heroArray.getString("Name");

                            String Image=heroArray.getString("Avatar");

                            String job_in_time=heroArray.getString("job_in_time");

                            String lunch_in_time=heroArray.getString("lunch_in_time");

                            String tea_in_time=heroArray.getString("tea_in_time");

                            String job_out_time=heroArray.getString("job_out_time");

                            String lunch_out_time=heroArray.getString("lunch_out_time");

                            String tea_out_time=heroArray.getString("tea_out_time");

                            department=heroArray.getString("Department");

                            allow_outside=heroArray.getString("Allow_outside");

                            Toast.makeText(LoginActivity.this, "Welcome "+uname, Toast.LENGTH_SHORT).show();

                            byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);

                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            Glide.with(LoginActivity.this)
                                    .load(decodedString)
                                    .asBitmap()
                                    .placeholder(R.drawable.user)
                                    .into(showImageView);

                            showCurrentLocation();

                            SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                            String time=sdft.format(new Date());

                            if(activity.equals("Work")) {
                                if(job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    workstatus="Start Job";
                                }
                                else if(!job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    workstatus="End Job";
                                }
                                else
                                {
                                    workstatus="Already Ended";
                                }

                                if (job_in_time.equals("00:00:00")) {
                                    stlog = 1;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                } else {
                                    try {
                                        Date current = new SimpleDateFormat("HH:mm:ss").parse(time);
                                        Date db = new SimpleDateFormat("HH:mm:ss").parse(job_in_time);

                                        long different = current.getTime() - db.getTime();

                                        Log.e("difference ", String.valueOf(different));
                                        if (different < 700000) {
                                            min = 1;
                                            //                                        Toast.makeText(LoginActivity.this, "wait 10 minute to logout", Toast.LENGTH_SHORT).show();
                                        } else {
                                            stlog = 0;
                                            conformation.setVisibility(View.VISIBLE);
                                            blink();
                                            showImageView.setVisibility(View.VISIBLE);
                                            lnedlogin.setVisibility(View.GONE);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }

                                if(job_out_time.equals("00:00:00"))
                                {
                                    stlogout=1;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }
                                else
                                {
                                    stlogout=0;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }

                            }
                            else if(activity.equals("Break"))
                            {
                                if(lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    workstatus="Start Break";
                                    String msgbreak="Please Start Break from your Computer";
                                    conformation.setText(msgbreak);

                                    showDialog(msgbreak);
                                }
                                else if(!lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    workstatus="End Break";
                                }
                                else
                                {
                                    workstatus="Already Ended";
                                }


                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(lunch_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    stlunch=1;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }
                                else
                                {
                                    stlunch=0;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }

                                if(lunch_out_time.equals("00:00:00"))
                                {
                                    stlunchout=1;
                                    blink();
                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }
                                else
                                {
                                    stlunchout=0;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);

                                }
                            }
                            else if(activity.equals("Tea Break"))
                            {
                                if(tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    workstatus="Start Break";
                                }
                                else if(!tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    workstatus="End Break";
                                }
                                else
                                {
                                    workstatus="Already Ended";
                                }

                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(tea_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    sttea=1;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }
                                else
                                {
                                    sttea=0;
                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }

                                if(tea_out_time.equals("00:00:00"))
                                {
                                    stteaout=1;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);
                                }
                                else
                                {
                                    stteaout=0;
                                    conformation.setVisibility(View.VISIBLE);
                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                    lnedlogin.setVisibility(View.GONE);

                                }
                            }





                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void checkwork(final String URL, final Bitmap image) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        Log.e("server",ServerResponse);
                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                            checkauthitication();
                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());

                SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                String time=sdft.format(new Date());


                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //compress the image to jpg format
                image.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            /*
            * encode image to base64 so that it can be picked by saveImage.php file
            * */
                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);
                Log.e("image",encodeImage);

                /*Log.e("email "+email,"\ndate "+date);
                Log.e("type "+status+"\nimage "+ main.getBytes().length,"\npunch_time "+time);*/

                // Adding All values to Params.
                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);
                params.put("lat", lattitude);
                params.put("long", longitude);
//                params.put("department", department);
                params.put("allow_outside", allow_outside);
                params.put("image", encodeImage);//encodeImage//String.valueOf(image)
                params.put("time", time);

                return params;
            }

        };

        MySingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);
       /* // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);*/
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle("Warning")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }

    private void checkauthitication() {
        lnemoji.setVisibility(View.VISIBLE);

        File fdelete = new File(imageFilePath);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + imageFilePath);
            } else {
                System.out.println("file not Deleted :" + imageFilePath);
            }
        }
        //emoji.setBackgroundResource(R.drawable.lunch_start);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Intent i=new Intent(LoginActivity.this,LoginActivity.class);
                i.putExtra("activity", activity);
                startActivity(i);
                finish();

                //Toast.makeText(TrackerActivity.this, "toast", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
        // stop the camera
        if(previewing){
            camera.stopPreview(); // stop preview using stopPreview() method
            previewing = false; // setting camera condition to false means stop
        }
        // condition to check whether your device have camera or not
        if (camera != null){
            try {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE); //EFFECT_SEPIA //applying effect on camera
                camera.setParameters(parameters); // setting camera parameters
                camera.setPreviewDisplay(surfaceHolder); // setting preview of camera
                camera.startPreview();  // starting camera preview

                previewing = true; // setting camera to true which means having camera
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
		/*int cameraId = 0;
		for(int i=0;i<Camera.getNumberOfCameras();i++){
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId,info);
			if(info.facing== Camera.CameraInfo.CAMERA_FACING_FRONT){
				cameraId = i;
				break;
			}
		}*/
        try
        {
            camera = Camera.open(1);
            //camera = Camera.open();   // opening camera
            camera.setDisplayOrientation(90);   // setting camera preview orientation
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Front camera not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        if (camera != null){
            try {
                camera.stopPreview();  // stopping camera preview
                camera.release();       // releasing camera
                camera = null;          // setting camera to null when left
                previewing = false;   // setting camera condition to false also when exit from application
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

           /* case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/



        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.with(getApplicationContext()).pauseRequests();
    }

}
